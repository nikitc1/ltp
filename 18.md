## 18. Алгоритмы сортировки. Пузырьковая сортировка. Шейкерная сортировка. Сортировка выбором. Сортировка вставками. Сортировка Хоара. Вычислительная сложность данных алгоритмов.

`Сортировка` – это перестановка элементов некоторой совокупности с целью расположить их в определенном порядке. 

### 18.1 Пузырьковая сортировка

Или `сортировка простыми обменами`. Принцип действий прост: обходим массив от начала до конца, попутно меняя местами неотсортированные соседние элементы. В результате первого прохода на последнее место «всплывёт» максимальный элемент. Теперь снова обходим неотсортированную часть массива (от первого элемента до предпоследнего) и меняем по пути неотсортированных соседей. Второй по величине элемент окажется на предпоследнем месте. Продолжая в том же духе, будем обходить всё уменьшающуюся неотсортированную часть массива, запихивая найденные максимумы в конец.


Алгоритм состоит в повторяющихся проходах по сортируемому массиву. На каждой итерации последовательно сравниваются соседние элементы, и, если порядок в паре неверный, то элементы меняют местами. За каждый проход по массиву как минимум один элемент встает на свое место, поэтому необходимо совершить не более `n−1` проходов, где `n` размер массива, чтобы отсортировать массив.


```cpp
void bubbleSort(int* a, int n) {
    for (int i = n - 1; i >= 0; i--) {
        for (int j = 0; j < i; j++) {
            if (a[j] > a[j+1]) {
                int tmp = a[j];
                a[j] = a[j + 1];
                a[j + 1] = tmp;
            }
        }
    }
}
```

__Сложность этого алгоритма:__ $`O(n^2)`$

### 18.2 Шейкерная сортировка

Начинается процесс как в «пузырьке»: выдавливаем максимум на самые задворки. После этого разворачиваемся на `180` и идём в обратную сторону, при этом уже перекатывая в начало не максимум, а минимум. Отсортировав в массиве первый и последний элементы, снова делаем кульбит. Обойдя туда-обратно несколько раз, в итоге заканчиваем процесс, оказавшись в середине списка.

Шейкерная сортировка работает немного быстрее чем пузырьковая, поскольку по массиву в нужных направлениях попеременно мигрируют и максимумы и минимумы. Улучшения, как говорится, налицо.


```cpp
void shakerSort(int* a, int n) {
    int left = 0;
    int right = n - 1;
    while (left < right) {
        for (int i = left; i < right; i++) {
            if (a[i] > a[i + 1]) {
                int tmp = a[i];
                a[j] = a[i + 1];
                a[i + 1] = tmp;
            }
        }
        right--;
        for (int i = right; i > left; i--) {
            if (a[i] < a[i - 1]) {
                int tmp = a[i];
                a[j] = a[i - 1];
                a[i - 1] = tmp;
            }
        }
        left++;
    }

    return arr;
}
```

__Сложность этого алгоритма:__ $`O(n^2)`$, но стремится она к `O(k⋅n)`, где `k` — максимальное расстояние элемента в неотсортированном массиве от его позиции в отсортированном массиве

### 18.3 Сортировка выбором

Для сортировки массива методом выбора от наименьшего до наибольшего элемента выполняются следующие шаги:

1. Начиная с элемента под индексом `0`, ищем в массиве наименьшее значение.
2. Найденное значение меняем местами с нулевым элементом.
3. Повторяем шаги `№1` и `№2` уже для следующего индекса в массиве (отсортированный элемент больше не трогаем).

Другими словами, мы ищем наименьший элемент в массиве и перемещаем его на первое место. Затем ищем второй наименьший элемент и перемещаем его уже на второе место после первого наименьшего элемента. Этот процесс продолжается до тех пор, пока в массиве не закончатся неотсортированные элементы.


![Alt text](https://purecodecpp.com/wp-content/uploads/2015/08/sortirovka-vyborom-c.jpg "Пример")



* В переменной `smallestIndex` хранится индекс наименьшего значения, которое мы нашли в этой итерации.
* Начинаем с того, что наименьший элемент в этой итерации - это первый элемент (индекс 0)
```cpp
void selectionSort(int* a, int n) {
    for (int i = 0; i < n - 1; i++) {
        int smallestIndex = i;
        // Затем ищем элемент поменьше в остальной части массива
        for (int j = i + 1; j < n; j++) {
        // Если мы нашли элемент, который меньше нашего наименьшего элемента,
            if (a[j] < a[smallestIndex]) {
                // то запоминаем его
                smallestIndex = j;
            }
        }
 
        // smallestIndex теперь наименьший элемент. 
        int tmp = a[i];
        a[i] = a[smallestIndex];
        a[smallestIndex] = tmp;
    }
}
```
__Сложность этого алгоритма:__ $`O(n^2)`$


### 18.4 Сортировка вставками

`Сортировка вставками` — достаточно простой алгоритм. Как в и любом другом алгоритме сортировки, с увеличением размера сортируемого массива увеличивается и время сортировки. Основным преимуществом алгоритма сортировки вставками является возможность сортировать массив по мере его получения.То есть имея часть массива, можно начинать его сортировать. В параллельном программирование такая особенность играет не маловажную роль.

Сортируемый массив можно разделить на две части — отсортированная часть и неотсортированная. В начале сортировки первый элемент массива считается отсортированным, все остальные — не отсортированные. Начиная со второго элемента массива и заканчивая последним, алгоритм вставляет неотсортированный элемент массива в нужную позицию в отсортированной части массива. Таким образом, за один шаг сортировки отсортированная часть массива увеличивается на один элемент, а неотсортированная часть массива уменьшается на один элемент.

Задача заключается в следующем: есть часть массива, которая уже отсортирована, и требуется вставить остальные элементы массива в отсортированную часть, сохранив при этом упорядоченность. Для этого на каждом шаге алгоритма мы выбираем один из элементов входных данных и вставляем его на нужную позицию в уже отсортированной части массива, до тех пор пока весь набор входных данных не будет отсортирован. Метод выбора очередного элемента из исходного массива произволен, однако обычно (и с целью получения устойчивого алгоритма сортировки), элементы вставляются по порядку их появления во входном массиве.

Так как в процессе работы алгоритма могут меняться местами только соседние элементы, каждый обмен уменьшает число инверсий на единицу. Следовательно, количество обменов равно количеству инверсий в исходном массиве вне зависимости от реализации сортировки. Максимальное количество инверсий содержится в массиве, элементы которого отсортированы по невозрастанию. Число инверсий в таком массиве $`\frac{n(n-1)}{2}`$

```cpp
void insertionSort(int* a, int n) {
    int temp, // временная переменная для хранения значения элемента сортируемого массива
    item; // индекс предыдущего элемента
    for (int i = 0; i < n; i++) {
        temp = a[i]; // инициализируем временную переменную текущим значением элемента массива
        prev_index = i-1; // запоминаем индекс предыдущего элемента массива
        // пока индекс не равен 0 и предыдущий элемент массива больше текущего 
        while(prev_index >= 0 && a[prev_index] > temp) {
            a[prev_index + 1] = a[prev_index]; // перестановка элементов массива
            a[prev_index] = temp;
            prev_index--;
        }
    }
}
```

__Сложность этого алгоритма:__ Алгоритм работает за `O(n+k)`, где `k` — число обменов элементов входного массива, равное числу инверсий. В среднем и в худшем случае — за $`O(n^2)`$. Минимальные оценки встречаются в случае уже упорядоченной исходной последовательности элементов, наихудшие — когда они расположены в обратном порядке.

### 18.5 Сортировка Хоара

__Как работает быстрая сортировка__

Схему алгоритма можно описать таким образом:

1. Выбрать `опорный` элемент в массиве — часто встречается вариант с центральным элементом.
2. Разделить массив на `две части` следующим образом: все элементы из `левой` части, которые `больше или равны` опорному, перекидываем в `правую`, аналогично, все элементы из `правой`, которые `меньше или равны` опорному кидаем в левую часть.
3. В результате предыдущего шага в левой части массива останутся элементы, которые меньше или равны центральному, а в правой — больше либо равны.
Наглядно это можно показать таким образом:

Здесь `pivot` - опорный элемент

|                |                    |                 |
|----------------|--------------------|-----------------|
| a[i] <= pivot  | pivot = a[size/2]  | a[i] >= pivot   |
|                |                    |                 |
4. Рекурсивно повторяем действие для левой и правой части массива.

Заходы в рекурсию прекратятся в тот момент, когда размер обоих частей будет меньше или равен единице.

```cpp
void hoareSort(int* a, int n) {
    hoareSort(a, 0, n - 1);
}

void hoareSort(int* a, int first, int last) {
    int temp;
    int startIndex = first;
    int endIndex = last;
    int pivot = a[(startIndex + endIndex) / 2]; //вычисление опорного элемента
    do {
        while (a[f] < pivot) f++;
        while (a[l] > pivot) l--;
        //перестановка элементов
        if (f<=l) {
            temp = a[startIndex];
            a[startIndex] = a[endIndex];
            a[endIndex]=temp;

            startIndex++;
            endIndex--;
        }
    } while (startIndex < endIndex);
    if (first < endIndex) hoareSort(a, first, endIndex);
    if (startIndex < last) hoareSort(a, startIndex, last);
}
```

__Сложность этого алгоритма:__

* Сложность в лучшем/среднем случае: $`O(n log n)`$
* Сложность в худшем случае случае: $`O(n^2)`$

* `Лучший случай`
Если в каждой итерации каждый из подмассивов будет делиться на два приблизительно равных по величине массива

* `Худший случай` - реализуется если каждый раз в качестве центрального элемента выбирается максимум или минимум входной последовательности. В этом случае каждое разделение дает подмассив размерами `1 и n-1` и при каждом рекурсивном вызове большой массив будет на 1 короче, чем предыдущий раз. В этом случае потребуется `n-1` операций разделения, а общее время работы составит $`O(n^2)`$ операцией, то есть сортировка будет выполняться за квадратичное время.